-- CreateTable
CREATE TABLE `Usuario` (
    `id` VARCHAR(191) NOT NULL,
    `nombres` VARCHAR(191) NOT NULL,
    `apellidos` VARCHAR(191) NOT NULL,
    `username` VARCHAR(191) NOT NULL,
    `password` VARCHAR(191) NOT NULL,
    `rol` ENUM('admin', 'jefe', 'bibliotecario') NOT NULL,
    `isActive` BOOLEAN NOT NULL,
    `cargo` VARCHAR(191) NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,

    UNIQUE INDEX `Usuario_username_key`(`username`),
    UNIQUE INDEX `Usuario_rol_key`(`rol`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Libro` (
    `id` INTEGER NOT NULL,
    `titulo` LONGTEXT NOT NULL,
    `tituloBusqueda` LONGTEXT NULL,
    `autores` LONGTEXT NULL,
    `numEdicion` VARCHAR(191) NULL,
    `editorial` VARCHAR(191) NULL,
    `encuadernacion` VARCHAR(191) NULL,
    `aEdicion` INTEGER NULL,
    `coleccion` VARCHAR(191) NULL,
    `fechaLanzamiento` VARCHAR(191) NULL,
    `numPaginas` VARCHAR(191) NULL,
    `dimensiones` VARCHAR(191) NULL,
    `tipoDocumento` VARCHAR(191) NULL,
    `isbn` VARCHAR(191) NULL,
    `idioma` VARCHAR(191) NULL,
    `pesoFisico` VARCHAR(191) NULL,
    `portadaFile` VARCHAR(191) NULL,
    `portadaSigb` LONGTEXT NULL,
    `notaGeneral` LONGTEXT NULL,
    `clasificacion` VARCHAR(191) NULL,
    `categoria` LONGTEXT NULL,
    `notaDeContenido` LONGTEXT NULL,
    `linkSigb` VARCHAR(191) NULL,
    `datosVerificados` BOOLEAN NOT NULL DEFAULT false,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Separaciones` (
    `id` VARCHAR(191) NOT NULL,
    `dni` VARCHAR(191) NOT NULL,
    `codigoEstudiante` VARCHAR(191) NOT NULL,
    `estadoSeparacion` ENUM('enEspera', 'cancelado', 'realizado') NOT NULL DEFAULT 'enEspera',

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `librosEnSeparacion` (
    `libroId` INTEGER NOT NULL,
    `separacionesId` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`libroId`, `separacionesId`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `LibroSolicitud` (
    `id` VARCHAR(191) NOT NULL,
    `titulo` VARCHAR(191) NOT NULL,
    `autores` VARCHAR(191) NOT NULL,
    `editorial` VARCHAR(191) NOT NULL,
    `encuadernacion` VARCHAR(191) NOT NULL,
    `aEdicion` INTEGER NOT NULL,
    `fechaLanzamiento` VARCHAR(191) NOT NULL,
    `idioma` VARCHAR(191) NOT NULL,
    `isbn` VARCHAR(191) NOT NULL,
    `numPaginas` VARCHAR(191) NOT NULL,
    `dimensiones` VARCHAR(191) NOT NULL,
    `pesoFisico` VARCHAR(191) NOT NULL,
    `portadaFile` VARCHAR(191) NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,
    `libroId` INTEGER NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `SolicitudDeConstancia` (
    `id` VARCHAR(191) NOT NULL,
    `nombres` VARCHAR(191) NOT NULL,
    `apellidos` VARCHAR(191) NOT NULL,
    `dni` VARCHAR(191) NOT NULL,
    `codigoEstudiante` VARCHAR(191) NOT NULL,
    `estadoSolicitud` VARCHAR(191) NOT NULL DEFAULT 'pendiente',
    `constanciaNum` INTEGER NULL,
    `constanciaYear` INTEGER NULL,
    `constanciaFecha` DATETIME(3) NULL,
    `evaluacionComentario` VARCHAR(191) NULL,
    `evaluadoPor` VARCHAR(191) NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,
    `libroSolicitudId` VARCHAR(191) NULL,

    UNIQUE INDEX `SolicitudDeConstancia_libroSolicitudId_key`(`libroSolicitudId`),
    UNIQUE INDEX `SolicitudDeConstancia_constanciaNum_constanciaYear_key`(`constanciaNum`, `constanciaYear`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `LibrosRecibidos` (
    `id` VARCHAR(191) NOT NULL,
    `solicitudId` VARCHAR(191) NOT NULL,
    `recibidoPor` VARCHAR(191) NOT NULL,
    `fechaRecibido` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,

    UNIQUE INDEX `LibrosRecibidos_solicitudId_key`(`solicitudId`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `UserSession` (
    `id` VARCHAR(191) NOT NULL,
    `username` VARCHAR(191) NOT NULL,
    `token` LONGTEXT NOT NULL,
    `ip` VARCHAR(191) NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `librosEnSeparacion` ADD CONSTRAINT `librosEnSeparacion_libroId_fkey` FOREIGN KEY (`libroId`) REFERENCES `Libro`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `librosEnSeparacion` ADD CONSTRAINT `librosEnSeparacion_separacionesId_fkey` FOREIGN KEY (`separacionesId`) REFERENCES `Separaciones`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `LibroSolicitud` ADD CONSTRAINT `LibroSolicitud_libroId_fkey` FOREIGN KEY (`libroId`) REFERENCES `Libro`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `SolicitudDeConstancia` ADD CONSTRAINT `SolicitudDeConstancia_libroSolicitudId_fkey` FOREIGN KEY (`libroSolicitudId`) REFERENCES `LibroSolicitud`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `LibrosRecibidos` ADD CONSTRAINT `LibrosRecibidos_solicitudId_fkey` FOREIGN KEY (`solicitudId`) REFERENCES `SolicitudDeConstancia`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
