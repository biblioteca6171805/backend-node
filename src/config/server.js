export default {
    biblioteca: {
        cantidadMaximaDeLibrosAPrestarPorEstudiante: 3,
        cantidadMaximaDeLibrosASepararPorEstudiante: 3,
        diasMaximosDePrestamoLibro: 3,
        diasMaximosDeSeparacionLibro: 3,
    }
}