import express from 'express';
import bodyParser from 'body-parser'
import morgan from 'morgan';
import cors from 'cors';
import solicitudConstanciaRouter from './routes/api/solicitud-constancia.routes.js';
import userRouter from './routes/api/user.routes.js';
import authRouter from './routes/api/auth.routes.js';
import libroRouter from './routes/api/libro.routes.js';
import separacionLibrosRouter  from './routes/api/separacion-libros.routes.js';
import adminPanel from './routes/api/panel-admin.routes.js';
import multer from 'multer';
import path from 'path';
import bibliotecaParametros from './config/server.js'

const app = express();
const upload = multer();
app.use(upload.any());
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(cors());
app.use(express.static('src/public/'))

import pino from 'pino-http';
import logger from 'pino';
app.use(morgan(':remote-addr - :remote-user :date ":method :url  :status ":referrer"'));
// app.use(pino())



app.get('/api/biblioteca-parametros',(req,res)=>{
    res.json(bibliotecaParametros.biblioteca)
})

// ruta solicitudContancia 
app.use('/api/solicitud-constancia/', solicitudConstanciaRouter);

// ruta auth;
app.use('/api/auth/', authRouter);

// ruta usuarios;
app.use('/api/user/', userRouter);

// ruta libros
app.use('/api/libros', libroRouter);


// ruta separacion de libros
app.use('/api/separacion-libros', separacionLibrosRouter);

// ruta panel de adminitracion
app.use('/api/admin-panel',adminPanel)


app.get('/api/doc', (req, res) => {

    if (req.query.token == "termostato") {
        let fullPath = path.join('./src/public');
        res.sendFile(`/html/doc.html`, { root: fullPath });
    } else {
        res.send(':(')
    }
})

// cualquier otra ruta
app.use(function (req, res) {
    console.log('El API no soporta este request. verificar que la ruta o el metodo sea el correcto')
    return res.status(501).json({
        statusCode: "notImplemented",
        message: 'El API no soporta este request.',
        details: `Metodo: ${req.method}, Ruta: ${req.path}`
    })
});


export default app;
