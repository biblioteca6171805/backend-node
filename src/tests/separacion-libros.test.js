import app from '../app.js'
import supertest from 'supertest';
import database from '../database/prisma-db-client.js'

beforeAll(async () => {
    // await database.usuario.deleteMany();
});
let separacion;


// USER -> Publico
test("POST /api/separacion-libros/crear-separacion | separar libros", async () => {
    let data = {
        dni: "70707070",
        codigoEstudiante: "121212",
        libros: "12117;12474;20104" 
    }
    let resultadoEsperado = {
        "statusCode": "success",
        "message": "separacion realizada exitosamente!"
    }

    await supertest(app).post(`/api/separacion-libros/crear-separacion`)
        .send(data)
        .expect(200)
        .then(async (response) => {
            // Check type and length
            // 
            expect(response).toEqual(expect.any(Object));

            // Check data
            expect(response.body.message).toBe(resultadoEsperado.message);
            expect(response.body.statusCode).toBe(resultadoEsperado.statusCode);
            separacion = response.body.data;
        });
});


// USER -> Bibliotecario
test("POST /api/separacion-libros/crear-separacion | separar libros", async () => {
    let data = {
        dni: "70707070",
        codigoEstudiante: "121212",
        libros: "12117;12474;20104" 
    }
    let resultadoEsperado = {
        statusCode: "success",
        message: "separacion realizada exitosamente!",
        // data:separacion
    }

    await supertest(app).post(`/api/separacion-libros/crear-separacion`)
        .send(data)
        .expect(200)
        .then(async (response) => {
            // Check type and length
            // 
            expect(response).toEqual(expect.any(Object));

            // Check data
            expect(response.body.message).toBe(resultadoEsperado.message);
            expect(response.body.statusCode).toBe(resultadoEsperado.statusCode);
            separacion = response.body.data;
        });
});


// USER -> Bibliotecario
test("GET /api/separacion-libros/get/separacion/:separacionId | get datos de separacion By Id", async () => {

    let resultadoEsperado = {
        statusCode: "success",
        message: "separacion encontrado",
        // data:[...]
    }
    // console.log(separacion);

    await supertest(app).get(`/api/separacion-libros/get/separacion/${separacion.id}`)
        .expect(200)
        .then(async (response) => {
            // Check type and length
            // 
            expect(response).toEqual(expect.any(Object));

            // Check data
            expect(response.body.message).toBe(resultadoEsperado.message);
            expect(response.body.statusCode).toBe(resultadoEsperado.statusCode);
        });
});


// USER -> Bibliotecario
test("POST /api/separacion-libros/evaluar-separacion | cambiarEstado", async () => {

    let data = {
        separacionId:separacion.id,
        nuevoEstado: 'realizado',
    //  nuevoEstado: enEspera | realizado | cancelado
    }

    let resultadoEsperado = {
        statusCode: "success",
        message: "separacion evaluada exitosamente!",
        // data:separacion
    }
    // console.log(separacion);

    await supertest(app).post(`/api/separacion-libros/evaluar-separacion`)
        .send(data)
        .expect(200)
        .then(async (response) => {
            // Check type and length
            // 
            expect(response).toEqual(expect.any(Object));

            // Check data
            expect(response.body.message).toBe(resultadoEsperado.message);
            expect(response.body.statusCode).toBe(resultadoEsperado.statusCode);
        });
})

// USER -> Bibliotecario
test("GET /api/separacion-libros/get/separaciones | get lista de separaciones en base a un estado", async () => {

    let data = {
    //  estadoSeparacion: enEspera | realizado | cancelado
    estadoSeparacion: "enEspera"
    }

    let resultadoEsperado = {
        statusCode: "success",
        message: `lista de separaciones con estado: ${data.estadoSeparacion}` 
        // data:separacion
    }
    await supertest(app).get(`/api/separacion-libros/get/separaciones`)
        .query(data)
        .expect(200)
        .then(async (response) => {
            // Check type and length
            // 
            expect(response).toEqual(expect.any(Object));

            // Check data
            expect(response.body.message).toBe(resultadoEsperado.message);
            expect(response.body.statusCode).toBe(resultadoEsperado.statusCode);
        });
})
