// import database from '../database/prisma-db-client.js';
import app from '../app.js'
import supertest from 'supertest';
import database from '../database/prisma-db-client.js'

beforeAll(async () => {
    await database.usuario.deleteMany();
});

// crear cuentas

test("POST /api/auth/create-account | crear cuenta admin", async () => {
    // 
    let data = {
        'username': 'admon',
        'password': 'passwordsaso',
        'passwordCheck': 'passwordsaso',
        'rol': 'admin',
        'nombres': 'don',
        'apellidos': 'ziu',
        'cargo': 'Dr. calamardo aleta'
    }

    let resultadoEsperado = {
        "statusCode": "success",
        "message": "cuenta creada correctamente!"
    }

    await supertest(app).post("/api/auth/create-account")
        .send(data)
        .expect(200)
        .then(async (response) => {
            // Check type and length
            expect(response).toEqual(expect.any(Object));

            // Check data
            expect(response.body.message).toBe(resultadoEsperado.message);
            expect(response.body.statusCode).toBe(resultadoEsperado.statusCode);
        });
});

// verificar que los usuarios ingresen los datos necesarios para registrarse


test("POST /api/auth/create-account | verificar datos requeridos", async () => {
    // 
    let data = {
        'username': 'admon',
        'password': 'passwordsaso',
        'passwordCheck': 'passwordsaso',
        'rol': 'admin',
        // 'nombres': 'don',
        'apellidos': 'ziu',
        'cargo': 'Dr. calamardo aleta'
    }

    let resultadoEsperado = {
        statusCode: "badRequest",
        message: 'campos requeridos: username, password, passwordCheck, rol, nombres, apellidos, cargo',
    }

    await supertest(app).post("/api/auth/create-account")
        .send(data)
        .expect(400)
        .then(async (response) => {
            expect(response).toEqual(expect.any(Object));
            expect(response.body.message).toBe(resultadoEsperado.message);
            expect(response.body.statusCode).toBe(resultadoEsperado.statusCode);
        });
});

test("POST /api/auth/create-account | crear cuenta jefe de biblioteca", async () => {
    // 
    let data = {
        'username': 'jefazobibliotecarioxd',
        'password': 'uno#TRES444',
        'passwordCheck': 'uno#TRES444',
        'rol': 'jefe',
        'nombres': 'don',
        'apellidos': 'ali',
        'cargo': 'Dr. calamardo aleta'
    }

    let resultadoEsperado = {
        "statusCode": "success",
        "message": "cuenta creada correctamente!"
    }

    await supertest(app).post("/api/auth/create-account")
        .send(data)
        .expect(200)
        .then(async (response) => {
            expect(response).toEqual(expect.any(Object));
            expect(response.body.message).toBe(resultadoEsperado.message);
            expect(response.body.statusCode).toBe(resultadoEsperado.statusCode);
        });
});

test("POST /api/auth/create-account | crear cuenta bibliotecario", async () => {
    // 
    let data = {
        'username': 'bibliotecario',
        'password': '123',
        'passwordCheck': '123',
        'rol': 'bibliotecario',
        'nombres': 'z',
        'apellidos': 'ziu',
        'cargo': 'Dr. bibliotecario elegante'
    }

    let resultadoEsperado = {
        "statusCode": "success",
        "message": "cuenta creada correctamente!"
    }

    await supertest(app).post("/api/auth/create-account")
        .send(data)
        .expect(200)
        .then(async (response) => {
            expect(response).toEqual(expect.any(Object));
            expect(response.body.message).toBe(resultadoEsperado.message);
            expect(response.body.statusCode).toBe(resultadoEsperado.statusCode);
        });
});

// verificar usuario unico
test("POST /api/auth/create-account | verfificar restriccion usuario unico", async () => {
    // 
    let data = {
        'username': 'bibliotecario',
        'password': '1234',
        'passwordCheck': '1234',
        'rol': 'bibliotecario',
        'nombres': 'z',
        'apellidos': 'ziu',
        'cargo': 'Dr. bibliotecario elegante'
    }

    let resultadoEsperado = {
        statusCode: "badRequest",
        message: 'el usuario ya existe',
    }

    await supertest(app).post("/api/auth/create-account")
        .send(data)
        .expect(400)
        .then(async (response) => {
            expect(response).toEqual(expect.any(Object));
            expect(response.body.statusCode).toBe(resultadoEsperado.statusCode);
            expect(response.body.message).toBe(resultadoEsperado.message);

        });
});

// verificar rol unico
test("POST /api/auth/create-account | verficar restriccion usuario unico por rol", async () => {
    // 
    let data = {
        'username': 'bibliotecario444',
        'password': '1234444',
        'passwordCheck': '1234444',
        'rol': 'bibliotecario',
        'nombres': 'z',
        'apellidos': 'ziu',
        'cargo': 'Dr. bibliotecario elegante'
    }

    let resultadoEsperado = {
        statusCode: "badRequest",
        message: "rol tomado por usuario previo",
    }

    await supertest(app).post("/api/auth/create-account")
        .send(data)
        .expect(400)
        .then(async (response) => {
            expect(response).toEqual(expect.any(Object));
            expect(response.body.statusCode).toBe(resultadoEsperado.statusCode);
            expect(response.body.message).toBe(resultadoEsperado.message);

        });
});

// test login
test("POST /api/auth/login | prueba login admin", async () => {
    // 
    let data = {
        'username': 'admon',
        'password': 'passwordsaso',
    }

    let resultadoEsperado = {
        "statusCode": "success",
        "message": "usuario logeado correctamente"
    }

    await supertest(app).post("/api/auth/login")
        .send(data)
        .expect(200)
        .then(async (response) => {
            expect(response).toEqual(expect.any(Object));
            expect(response.body.message).toBe(resultadoEsperado.message);
            expect(response.body.statusCode).toBe(resultadoEsperado.statusCode);
            expect(response.body.data).toHaveProperty('token');
        });
});

test("POST /api/auth/login | prueba login bibliotecario", async () => {
    // 
    let data = {
        'username': 'bibliotecario',
        'password': '123',
    }

    let resultadoEsperado = {
        "statusCode": "success",
        "message": "usuario logeado correctamente"
    }

    await supertest(app).post("/api/auth/login")
        .send(data)
        .expect(200)
        .then(async (response) => {
            expect(response).toEqual(expect.any(Object));
            expect(response.body.message).toBe(resultadoEsperado.message);
            expect(response.body.statusCode).toBe(resultadoEsperado.statusCode);
            expect(response.body.data).toHaveProperty('token');
        });
});

test("POST /api/auth/login | prueba login jefe de biblioteca", async () => {
    // 
    let data = {
        'username': 'jefazobibliotecarioxd',
        'password': 'uno#TRES444',
    }

    let resultadoEsperado = {
        "statusCode": "success",
        "message": "usuario logeado correctamente"
    }

    await supertest(app).post("/api/auth/login")
        .send(data)
        .expect(200)
        .then(async (response) => {
            expect(response).toEqual(expect.any(Object));
            expect(response.body.message).toBe(resultadoEsperado.message);
            expect(response.body.statusCode).toBe(resultadoEsperado.statusCode);
            expect(response.body.data).toHaveProperty('token');
        });
});

/*

// app.test.js
const request = require('supertest');
const app = require('./app');

describe('API Endpoint Tests', () => {
  it('GET /api/greet should return a greeting message', async () => {
    const response = await request(app).get('/api/greet');

    expect(response.status).toBe(200);
    expect(response.body).toEqual({ message: 'Hello, World!' });
  });
});


*/