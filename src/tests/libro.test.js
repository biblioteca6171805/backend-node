import app from '../app.js'
import supertest from 'supertest';
import database from '../database/prisma-db-client.js'

beforeAll(async () => {
    // await database.usuario.deleteMany();
});

test("GET /api/libros/get/:libroId | get libro por id", async () => {

    let libroId = 12117;

    let resultadoEsperado = {
        "statusCode": "success",
        "message": "libro encontrado"
    }

    await supertest(app).get(`/api/libros/get/${libroId}`)
        .expect(200)
        .then(async (response) => {
            // Check type and length
            // 
            expect(response).toEqual(expect.any(Object));

            // Check data
            expect(response.body.message).toBe(resultadoEsperado.message);
            expect(response.body.statusCode).toBe(resultadoEsperado.statusCode);
        });
});



test("GET /api/libros/search/query | buscar libro por etc etc etc", async () => {
    // /api/libros/search/query?autores=joyanes&titulo=uml&page=1&limit=50

    let data = {
        titulo: null,
        tituloBusqueda: null,
        autores: null,
        editorial: null,
        encuadernacion: null,
        clasificacion: null,
        fechaLanzamiento: null,
        idioma: null,
        isbn: null,
        numPaginas: null,
        dimensiones: null,
        pesoFisico: null,
        signatura: null,
        notaGeneral: null,
        notaDeContenido: null,
        notaGeneral: null,
        limit: null, // requerido
        page: null, // requerido
    }

    data.titulo = "aplicados a la ingenieria"
    data.page = 1;
    data.limit = 10;

    let resultadoEsperado = {
        "statusCode": "success",
        "message": "libros encontrados"
    }

    await supertest(app).get("/api/libros/search/query")
        .query(data)
        .expect(200)
        .then(async (response) => {
            // Check type and length
            // 
            expect(response).toEqual(expect.any(Object));

            // Check data
            expect(response.body.message).toBe(resultadoEsperado.message);
            expect(response.body.statusCode).toBe(resultadoEsperado.statusCode);
            console.log('libros totales', response.body.data.resultadosTotales);
        });
});
