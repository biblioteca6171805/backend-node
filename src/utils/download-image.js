import {v4 as uuidv4} from 'uuid'
import fs from 'fs';
import asyncShell from './async-shell.js';
export default async function downloadImage(url, downloadPath) {
    //creacion de imagen 
    try {
        let response;
        response = await fetch(url);
        const blob = await response.blob();
        const arrayBuffer = await blob.arrayBuffer();
        const buffer = Buffer.from(arrayBuffer);


        // let filename = `${uuidv4()}.png`;
        let filename = `exturl-${new Date().toISOString().split('.')[0]}-${uuidv4()}`;
        fs.writeFileSync(downloadPath + filename + '.png', buffer);
        // let filePath = downloadPath + filename
        // let { fileTypeFromFile } = require('file-type')

        let mime = await asyncShell(`file -b --mime-type ${downloadPath + filename}.png`);
        if (mime.indexOf('image') == -1) {
            console.log('error: download image mime type', mime);
            console.log('not image', mime)
            return { error: 'el archivo no es una imagen', filename: null }
        }

        let resize = await asyncShell(`convert -resize 500 ${downloadPath}${filename}.png PNG:${downloadPath}${filename}.png`);
        //console.log(resize);
        if (resize.length > 0) {
            return { error: 'error al tratar de reducir el peso de la imagen', filename: null }
        }
        // else {} // 200 resized ok

        //     fs.unlink(`${downloadPath}${filename}`, (deleteErr) => {
        //         if (deleteErr) console.error(deleteErr)
        //     });
        // return ;
        return { error: null, filename: filename + '.png' }
    }

    // fs.renameSync(`${filename}.png`, __dirname + '/app/public/img/uploads/' + `${filename}.png`);
    catch (err) {
        console.log(err)
        return { error: 'la imagen no se pudo descargar', filename: null }
    }


}