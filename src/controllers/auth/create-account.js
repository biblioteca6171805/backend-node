import database from '../../database/prisma-db-client.js';
import 'dotenv/config'
import bcrypt from 'bcrypt';
import auth from '../../middlewares/auth.js';


export default async function createAccount(req, res) {
    let { username, password, passwordCheck, rol, nombres, apellidos, codigoEstudiante, cargo } = req.body;

    if (!(username && password && passwordCheck && rol && nombres && apellidos && cargo)) {
        console.log({ message: 'campos requeridos: username, password, passwordCheck, rol, nombres, apellidos, cargo' })
        return {
            statusCode: "badRequest",
            message: 'campos requeridos: username, password, passwordCheck, rol, nombres, apellidos, cargo',
        }
    }

    let rolesAdmitidos = auth.Roles.All;
    //let rolesAdmitidos = ['jefe', 'estudiante', 'admin', 'bibliotecario'];

    if (!(password === passwordCheck)) {
        console.log({
            message: 'password y passwordCheck no son iguales',
        })
        return {
            statusCode: "badRequest",
            message: 'password y passwordCheck no son iguales',
        }
    }

    if (!rolesAdmitidos.includes(rol)) {
        return {
            statusCode: "badRequest",
            message: "rol no admitido. roles admitidos: " + rolesAdmitidos.join(' '),
        }
    }


    let usernameExists = await database.usuario.findFirst({ where: { username: username } });
    if (usernameExists) {
        return {
            statusCode: "badRequest",
            message: 'el usuario ya existe',
        }
    }

    let rolex = await database.usuario.findFirst({ where: { rol: rol } });
    if (rolex) {

        return {
            statusCode: "badRequest",
            message: "rol tomado por usuario previo",
        }
    }
    bcrypt.hash(password, 10).then(function (hashedPassword) {
        rol = rol.toLowerCase()
        const insertData = { nombres, apellidos, username, password: hashedPassword, rol, cargo, isActive: true };
        if (codigoEstudiante) {
            insertData.codigoEstudiante = codigoEstudiante;
        }
        // console.log('datos a ser ingresados para usuario nuevo', insertData)

        database.usuario.create({ data: insertData })
            .then((newUser) => {
                // const newUser = { id: result.id, emailId:result.emailId, name: result.name, role: result.role};
                console.log('usuario creado', newUser);
                res.status(200).json({
                    statusCode: 'success',
                    message: 'cuenta creada correctamente!',
                });
            })
            .catch(err => {
                console.log('sending error', err);
                return {
                    statusCode: "badRequest",
                    message: "no se pudo agregar usuario",
                    details: err
                }
            });
    });
};
