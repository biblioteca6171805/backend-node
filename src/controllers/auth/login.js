import database from '../../database/prisma-db-client.js';
import 'dotenv/config'
import bcrypt from 'bcrypt';
import auth from '../../middlewares/auth.js';

export default async function login(req, res) {
    const { username, password } = req.body;

    console.log(req.body)

    if (!username || !password) {
        return res.status(400).json({
            statusCode: "badRequest",
            message: "faltan valores para usuario y password",
        })
    }

    database.usuario.findUnique({ where: { username: username } })
        .then(async (result) => {
            // console.log(result);
            if (!result) {
                console.log("usuario o password incorrecto");
                return res.status(400).json({
                    statusCode: "badRequest",
                    message: "usuario o password incorrecto",
                })

            }

            const hashedPassword = result.password;
            let isPasswordCorrect;

            try {
                isPasswordCorrect = await bcrypt.compare(password, hashedPassword);
                console.log(isPasswordCorrect);
            } catch (errBcrypt) {
                console.log(errBcrypt);
                console.log({ message: "no se puedo encontrar el password o el usuario" });
                return res.status(400).json({ statusCode: "badRequest", message: "no se pudo encontrar el password del usuario", });
            }

            // Wrong password given
            if (!isPasswordCorrect) {
                console.log("usuario o password incorrecto");
                return res.status(401).json({ statusCode: "unauthorized", message: "usuario o password incorrecto", });
            }

            //User authenticated
            const user = result;
            delete user.password;

            const token = auth.issueToken(user);
            console.log('usuario logeado correctamente', user.username);
            database.sessionLogs.create({
                data: {
                    username: user.username,
                    ip: req.socket.remoteAddress
                }
            }).then(response => {
                res.status(200).json({
                    statusCode: 'success',
                    message: 'usuario logeado correctamente',
                    data: { token }
                });
                //return res.status(200).json({ ...user, token });
            })
        })
        .catch(err => {
            console.log('error de servidor', err);
            return res.status(400).json({ statusCode: "badRequest", message: "no se pudo encontrar los detalles de la cuenta :(", });

        });
}