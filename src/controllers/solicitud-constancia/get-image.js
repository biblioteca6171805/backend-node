import database from '../../database/prisma-db-client.js';
import path from 'path';
export default async function getImage(req, res, next) {
    let solicitudId = String(req.params.solicitudId);
    let solicitudImageDirectory = path.join('./src/storage/database/solicitud/images')
    database.solicitudDeConstancia.findUnique({ where: { id: solicitudId }, include: { libroSolicitud: true } })
        .then(solicitud => {
            // console.log(solicitud);
            if (solicitud) {
                res.setHeader('content-type', 'image/png');
                // res.sendFile(`${solicitudImageDirectory}/${solicitud.libroSolicitud.portadaFile}`);
                res.sendFile(`${solicitud.libroSolicitud.portadaFile}`,{root:solicitudImageDirectory});
            }
            else {
                console.log('solicitud no encontrada en db', solicitudId)
                return res.status(404).json({
                    statusCode: 'notFound',
                    message: 'solicitud no encontrada en db',
                    details: null
                })

            }
        })
        .catch(err => {
            console.log('server error', err)
            return res.status(500).json({
                statusCode: 'serverError',
                message: 'error de servidor',
            })

        })
}