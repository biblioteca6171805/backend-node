import database from '../../database/prisma-db-client.js';
import 'dotenv/config'
import path from 'path';
import utils from 'util';
import fs from 'fs';
import hb from 'handlebars';
import puppeteer from 'puppeteer';
import { exec } from 'child_process';
const readFile = utils.promisify(fs.readFile)

async function generatePdf(res, data) {
    let host = `http://${process.env.HOST}:${process.env.PORT}`;
    let meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
    let mesesAbreviados = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic',];

    let timezone = "T00:00:00.000-05:00";
    let libroFechaLanzamiento = new Date(data.libroFechaLanzamiento + timezone);
    data.libroFechaLanzamiento = `${String(libroFechaLanzamiento.getDate()).padStart(2, '0')} ${mesesAbreviados[libroFechaLanzamiento.getMonth()]}  ${libroFechaLanzamiento.getFullYear()}`

    // fecha de aceptacion de constancia
    let constanciaFecha = new Date(data.constanciaFecha).toLocaleString("es-CO", { timeZone: "America/Bogota" }).split(', ')[0].split('/');
    data.fechaGeneracionConstancia = `${String(constanciaFecha[0]).padStart(2, '0')} de ${meses[Number(constanciaFecha[1]) - 1]} del ${Number(constanciaFecha[2])}`

    // actualizacion de datos de entrada
    data.nombres = data.nombres.toUpperCase().trim();
    data.apellidos = data.apellidos.toUpperCase().trim();
    let title;
    if (data.libroTitulo.slice(-1)[0] == '.') {
        title = data.libroTitulo.substr(0, data.libroTitulo.length - 1);
    }
    // check constanciaNum
    if (data.constanciaNum < 100) {
        let ceros = 3 - String(data.constanciaNum).length;
        // console.log(ceros);
        data.constanciaNum = "0".repeat(ceros) + String(data.constanciaNum);
    }
    data.libroPortadaFile = `${host}/api/solicitud-constancia/solicitud/${data.id}/imagen`;
    console.log(data.libroPortadaFile);
    data.host = host;

    async function getTemplateHtml() {
        // let pdfPath = path.join('./src/storage/app');
        console.log("Loading template file in memory")
        try {
            // const invoicePath = path.resolve("solicitud-constancia/templatepdf.html");
            // console.log(pdfPath)
            const invoicePath = path.resolve("./src/storage/app/templatepdf.html");
            return await readFile(invoicePath, 'utf8');
        } catch (err) {
            return Promise.reject("Could not load html template");
        }
    }

    async function generatePdf(data) {
        let fullPath = path.join('./src/storage/database/solicitud/pdf');
        getTemplateHtml().then(async (res) => {
            console.log("Compiling the template with handlebars")
            const template = hb.compile(res, { strict: true });
            const result = template(data);
            const html = result;
            const browser = await puppeteer.launch({ headless: "new" });
            const page = await browser.newPage()
            await page.setContent(html)
            await page.pdf({ path: fullPath + `/${data.id}.pdf`, format: 'A4' })
            await browser.close();
            console.log("PDF Generated")

        }).then(() => {
            console.log(200, 'pdf generado correctamente');
            // custom pdf title
            if (data.descargaAutomatica === 'si') {
                res.setHeader('Content-Disposition', `attachment; filename=Constancia ${data.constanciaNum}-${data.constanciaYear} CodigoEstudiante ${data.codigoEstudiante}.pdf`);
            }

            exec(`exiftool -Title="Constancia ${data.constanciaNum}-${data.constanciaYear}  codigoEstudiante ${data.codigoEstudiante}" -Author="${data.nombres} ${data.apellidos}" -Subject="PDF Metadata" -overwrite_original ` + fullPath + `/${data.id}.pdf`, (err, stdout, stderr) => {
                console.log(err)
                res.sendFile(`/${data.id}.pdf`, { root: fullPath });
            });
        }).catch(err => {
            console.error(err)
            return res.status(500).json({
                statusCode: "serverError",
                message: "error en la generacion del pdf",
            })
        });
    }
    generatePdf(data);
}

export default async function getPdfFromSolicitud(req, res, next) {
    let solicitudId = String(req.params.solicitudId)
    database.solicitudDeConstancia.findUnique({ where: { id: solicitudId }, include: { libroSolicitud: true } })
        .then(solicitud => {
            console.log(solicitud);
            if (!solicitud) {
                console.log('solicitud no encontrada', solicitudId);
                return res.status(404).json({
                    statusCode: 'notFound',
                    message: 'solicitud no encontrada en db',
                })
            }
            if (solicitud.estadoSolicitud !== "aceptado") {
                console.log('estado solicitud != aceptado', solicitudId)
                return res.status(405).json({
                    statusCode: "notAllowed",
                    message: "solo se puede generar el pdf de las solicitudes aceptadas",
                });
            }
            for (let k of Object.keys(solicitud.libroSolicitud)) {
                solicitud['libro' + k[0].toUpperCase() + k.substr(1)] = solicitud.libroSolicitud[k];
            }
            solicitud.descargaAutomatica = req.query.descargaAutomatica;
            generatePdf(res, solicitud);

        })
        .catch(err => {
            console.log(err);
            return res.status(500).json({
                statusCode: "serverError",
                message: "error en la generacion del pdf",
            })
        })
} 