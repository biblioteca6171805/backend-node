import database from '../../database/prisma-db-client.js';
export default async function verEstadoSolicitud(req, res) {
    let codigoEstudiante = req.query.codigoEstudiante;
    let dni = req.query.dni
    if (!(codigoEstudiante && dni)) {
        console.log({
            error: 'parametros insuficientes para la busqueda',
        });
        return res.status(400).json({
            statusCode: "badRequest",
            message: 'parametros insuficientes para la busqueda de solicitud',

        })
    }
    // console.log('params',req.params);
    database.solicitudDeConstancia.findMany({
        where: {
            codigoEstudiante: codigoEstudiante,
            dni: dni
        },
        include: {
            libroSolicitud: true,
        }
    }).then(data => {
        return res.status(200).json({
            statusCode: "success",
            message: "lista de solicitudes encontradas",
            data: data,
        })
    }).catch(error => {
        console.log(error)
        return res.status(500).json({
            statusCode: "serverError",
            message: "error al buscar en db",
        })
    })
}