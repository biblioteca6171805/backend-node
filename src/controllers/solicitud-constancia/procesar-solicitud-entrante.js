import { validationResult } from 'express-validator';
import asyncShell from '../../utils/async-shell.js';
import fs from 'fs';
import { v4 as uuidv4 } from 'uuid'
import path from 'path';
import database from '../../database/prisma-db-client.js';
export default async function procesarSolicitudEntranteB64(req, res) {

    let errors = validationResult(req).array();
    let data = req.body;
    let libroExistente;
    if (errors.length > 0) {
        console.error(400, errors.length);
        return res.status(400).json({
            statusCode: "badRequest",
            message: "error en la validacion de datos",
            details: errors
        });
    }

    let inputLibro = {
        titulo: null,
        autores: null,
        editorial: null,
        encuadernacion: null,
        aEdicion: null,
        fechaLanzamiento: null,
        idioma: null,
        isbn: null,
        numPaginas: null,
        dimensiones: null,
        pesoFisico: null,
        portadaFile: null,
    }

    let libro
    if (data.libroId) {
        // verificar que el libro existe
        libro = await database.libro.findUnique({
            where: {
                id: Number(data.libroId),
                datosVerificados: true
            }
        })
        if (!libro) {
            return res.status(400).json({
                statusCode: "badRequest",
                message: "el libro no existe, porfavor ingrese los detalles del nuevo libro"
            })
        }
        libroExistente = true;
    }
    // verificar que existen todos los datos necesarios.
    // el estudiante no puede enviar solicitud si tiene solicitudes pendientes o ya aceptadas.
    let result = await database.solicitudDeConstancia.findFirst({
        where: {
            codigoEstudiante: data.codigoEstudiante,
            estadoSolicitud: {
                notIn: ['rechazado']
            }
        }
    });
    if (result) {
        return res.status(405).json({
            statusCode: "notAllowed",
            message: "No se puede realizar la solicitud en este momento. revise el estado de sus solicitudes previas",
        });
    }

    // All good
    // actualizacion de datos de entrada
    data.nombres = data.nombres.toUpperCase().trim();
    data.apellidos = data.apellidos.toUpperCase().trim();
    data.libroAEdicion = Number(data.libroAEdicion)
    data.libroNumPaginas = data.libroNumPaginas

    // limitar los datos de registro de la solicitud
    let inputData = {
        nombres: null,
        apellidos: null,
        dni: null,
        codigoEstudiante: null,
    }

    for (let key of Object.keys(inputData)) {
        inputData[key] = data[key];
    }

    // hasta aqui todos los datos de entrada estan listos para ser procesados por la base de datos.
    // agregar libro a la tabla libros.
    //libro nuevo 
    // processing base64 portada image
    let solicitudImageDirectory = path.join('./src/storage/database/solicitud/images')
    let base64String = data.libroPortadaFile;
    let base64Image = base64String.split(';base64,').pop();
    let filename = `base64-${new Date().toISOString().split('.')[0]}-${uuidv4()}`;
    fs.writeFileSync(`${solicitudImageDirectory}/${filename}.png`, base64Image, { encoding: 'base64' });

    let mime = await asyncShell(`file -b --mime-type ${solicitudImageDirectory}/${filename}.png`);
    // console.log('download image mime type', mime);
    if (mime.indexOf('image') == -1) {
        console.log('not an image', mime)
        return res.status(400).json({
            statusCode: "badRequest",
            message: "error en la validacion de datos de la solicitud",
            details: [{
                type: "file or url",
                msg: "el archivo no es una imagen",
                path: "libroPortadaFile",
                location: "body"
            }]
        });

    }
    let resize = await asyncShell(`convert -resize 280 ${solicitudImageDirectory}/${filename}.png PNG:${solicitudImageDirectory}/${filename}.png`);
    // resizing image/
    if (resize.length > 0) {
        return res.status(400).json({
            statusCode: "badRequest",
            message: "error en la validacion de datos de la solicitud",
            details: [{
                type: "file or url",
                msg: "no se pudo procesar la imagen",
                path: "libroPortadaFile",
                location: "body"
            }]
        });

    }
    // image saved successfully;
    data.libroPortadaFile = filename + '.png';
    for (let key of Object.keys(inputLibro)) {
        inputLibro[key] = data['libro' + key[0].toUpperCase() + key.substr(1)];
    }
    if (libroExistente) {
        inputLibro.libroId = libro.id;
    }
    inputData.libroSolicitud = { create: inputLibro };
    // console.log(inputData);
    database.solicitudDeConstancia.create({
        data: inputData,
        include: {
            libroSolicitud: true
        }
    }).then(async (solicitud) => {
        // TODO: devolver solo los datos necesarios ya que este es un metodo publico y no creo que sea recomendable mostrar todos los detalles de la solicitud o talvez si....
        console.log('registrado correctamente', solicitud.id);
        return res.json({
            statusCode: "success",
            message: "solicitud ingresada correctamente",
            data: solicitud
        })
    }).catch(err => {
        console.log(500, err)

        return res.status(500).json({
            statusCode: "serverError",
            message: "error al registrar solicitud en db",
        })
    })

}