import database from '../../database/prisma-db-client.js';
import path from 'path';
import 'dotenv/config'
export default async function evaluarSolicitud(req, res) {
    let acciones = ['aceptar', 'rechazar', 'eliminar'];
    let solicitudId = req.params.solicitudId
    let accion = req.body.accion;
    let comentario = req.body.comentario;
    if (!accion) {
        return res.status(400).json({
            statusCode: 'badRequest',
            message: 'valor para {accion} no encontrado'
        });
    }
    if (!acciones.includes(accion)) {
        return res.status(400).json({
            statusCode: 'badRequest',
            message: `${accion} no es una accion aceptable (aceptar, rechazar, eliminar)`
        });
    }
    // a este punto existe accion valida
    // ahora a vertificar que existe la solicitud;
    let jefe = await database.usuario.findUnique({where:{id: req.user.id }});
    let solicitud;
    try {
        solicitud = await database.solicitudDeConstancia.findUnique({ where: { id: String(solicitudId) } });
        if (!solicitud) { // no se encontro nada xd
            console.log('solicitud no encontrada en la base de datos', solicitudId)
            return res.status(404).json({
                statusCode: 'notFound',
                message: 'solicitud no encontrada en db',
            })
        }
        comentario = comentario ?? solicitud.evaluacionComentario;
    } catch (err) {
        console.log(err)
        return res.status(500).json({
            statusCode: "serverError",
            message: "error al ubicar solicitud en db",
        })
    }

    if (accion === 'rechazar') {
        if (solicitud.estadoSolicitud == 'aceptado') {
            return res.status(400).json({
                statusCode: 'badRequest',
                message: `accion no permitida, la solicitud ya fue aceptada`,
                data: solicitud
            });
        }
        database.solicitudDeConstancia.update({
            where: { id: String(solicitudId) },
            data: {
                estadoSolicitud: 'rechazado',
                evaluacionComentario: comentario,
                evaluadoPor: jefe.cargo,
                constanciaNum: null,
                constanciaYear: null,
                constanciaFecha: null,
            }, include: { libroSolicitud: true }
        }).then(async (solicitud) => {

            // let ejemplares = await database.libroEjemplar.findMany({ where: { libroId: solicitud.libro.libroId } });
            // if (ejemplares.length == 0) { // 
            //     let libroData = await database.libro.update({ where: { id: solicitud.libro.id }, data: { datosVerificados: false } })
            // }
            console.log(solicitud.id)
            return res.status(200).json({
                statusCode: "success",
                message: "solicitud rechazada correctamente",
                data: solicitud
            })

        }).catch(err => {
            console.log(err);
            return res.status(500).json({
                statusCode: "serverError",
                message: "error al actualizar solicitud en db",
            })

        })
    }
    else if (accion == 'aceptar') {
        let fechaActualDate = new Date().toLocaleString("es-CO", { timeZone: "America/Bogota" }).split(', ')[0].split('/');
        fechaActualDate = fechaActualDate.map((val) => { return Number(val) })
        let fechaActual = {
            dia: String(fechaActualDate[0]).padStart(2, '0'),
            mes: String(fechaActualDate[1] - 1).padStart(2, '0'),
            year: fechaActualDate[2]
        };
        let solicitud = await database.solicitudDeConstancia.findUnique({ where: { id: String(solicitudId) }, include: { libroSolicitud: true } })
        if (solicitud.estadoSolicitud === 'aceptado') {
            console.log('solicitud aceptada previamente');
            return res.status(200).json({
                statusCode: "success",
                message: "solicitud aceptada previamente",
                data: solicitud
            })
        }
        let constanciaNumMax = await database.solicitudDeConstancia.findFirst({
            where: { constanciaYear: { equals: Number(fechaActual.year) } },
            orderBy: { constanciaNum: 'desc' }
        });

        let numeroDeConstancia = constanciaNumMax ? Number(constanciaNumMax.constanciaNum + 1) : 1;


        database.solicitudDeConstancia.update({
            where: { id: String(solicitudId) },
            data: {
                estadoSolicitud: 'aceptado',
                constanciaNum: numeroDeConstancia,
                constanciaYear: fechaActual.year,
                constanciaFecha: (new Date()).toISOString(),
                evaluacionComentario: comentario,
                evaluadoPor: jefe.cargo
            }, include: { libroSolicitud: true }
        }).then(async (solicitud) => {
            console.log(solicitud.id)
            // let libroAceptado = await database.libro.update({ where: { id: solicitud.libro.id }, data: { datosVerificados: true } })
            // solicitud.libro = libroAceptado;
            return res.status(200).json({
                statusCode: "success",
                message: "solicitud aceptada correctamente",
                data: solicitud
            })
        }).catch(err => {
            console.log(err);
            return res.status(500).json({
                statusCode: "serverError",
                message: "error al actualizar estado de solicitud a aceptado en db",
            })

        })
    }
    else {
        return res.status(404).json({});
    }
}