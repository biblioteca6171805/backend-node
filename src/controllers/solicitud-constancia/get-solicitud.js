import database from '../../database/prisma-db-client.js';
export default async function getSolicitud(req, res) {
    // console.log(req.params)
    let solicitudId = String(req.params.solicitudId);

    database.solicitudDeConstancia.findUnique({
        where: {
            id: solicitudId
        },
        include: {
            libroSolicitud: true
        }
    }).then(data => {
        // console.log(data);
        if (data) {
            console.log(200, data.id)
            return res.json({
                statusCode: "success",
                message: "datos de solicitud encontrado",
                data: data
            })
        }
        else {
            console.log('solicitud no encontrada en la base de datos')
            return res.status(404).json({
                statusCode: "notFound",
                message: 'solicitud no encontrada en db',
            })
        }
    }).catch(error => {
        console.log(error)
        return res.status(500).json({
            statusCode: "serverError",
            message: "error al buscar solicitud en db",
        })
    })
}