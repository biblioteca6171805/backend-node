import { check } from 'express-validator';
// import { check, validationResult } from 'express-validator';

let solicitudValidator = [
    check('nombres').not().isEmpty(),
    check('apellidos').not().isEmpty(),
    check('dni').not().isEmpty(),
    check('codigoEstudiante').not().isEmpty(),
    check('libroTitulo').not().isEmpty(),
    check('libroAutores').not().isEmpty(),
    check('libroEditorial').not().isEmpty(),
    check('libroEncuadernacion').not().isEmpty(),
    check('libroAEdicion').not().isEmpty(),
    check('libroFechaLanzamiento').not().isEmpty(),
    check('libroIdioma').not().isEmpty(),
    check('libroNumPaginas').not().isEmpty(),
    check('libroDimensiones').not().isEmpty(),
    check('libroPesoFisico').not().isEmpty(),
    check('libroPortadaFile').not().isEmpty(),
]


export default solicitudValidator;