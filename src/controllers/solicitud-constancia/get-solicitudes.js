import database from '../../database/prisma-db-client.js';

export default async function getSolicitudes(req, res) {

    let estados = ['pendiente', 'rechazado', 'aceptado'];
    let estado = req.query.estadoSolicitud;
    if (estado && !estados.includes(estado)) {
        console.log({ message: `estado ${estado} no valido` })
        return res.status(400).json({
            statusCode: "badRequest",
            message: `estado ${estado} no valido`,
        })

    }
    // estado ok
    database.solicitudDeConstancia.findMany({
        where: {
            estadoSolicitud: estado // if estado == undefined => estadoSolictud All
        },
        orderBy: [{
            updatedAt: 'desc'
        }],
        include: {
            libroSolicitud: true
        }
    }).then(data => {
        console.log(200, data.length);
        return res.json({
            statusCode: "success",
            message: estado ? `lista de solicitudes con estado: ${estado}` : "lista completa de solicitudes",
            data: data
        })
    }).catch(error => {
        console.log({ error: error })
        return res.status(500).json({
            statusCode: "serverError",
            message: "error al buscar las solicitudes en db",
        })

    })
}