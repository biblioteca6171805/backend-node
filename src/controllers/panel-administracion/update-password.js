import database from '../../database/prisma-db-client.js';
import bcrypt from 'bcrypt';
import crypto from 'crypto'
import fs from 'fs';
import {exec} from 'child_process';
import 'dotenv/config'


async function regenerateToken() {
    process.env.JWT_SECRET = crypto.randomUUID()
    console.log('eh');


    const envPath = '.env';

    // Lee el contenido del archivo .env
    const envContent = fs.readFileSync(envPath, 'utf-8');

    // Cambia el valor de MY_VARIABLE
    let indexToken = envContent.indexOf("JWT_SECRET");
    const nuevoContenido = envContent.replace(`${envContent.substr(indexToken,48)}"`, `JWT_SECRET="${crypto.randomUUID()}"`);

    // Escribe el nuevo contenido de vuelta al archivo .env
    fs.writeFileSync(envPath, nuevoContenido);
    exec('pm2 reload '+ process.env.PM2_NAME)
    // console.log(nuevoContenido);

}
export default async function updatePassword(req, res) {
    database.usuario.findUnique({
        where: {
            username: String(req.body.username)
        }
    }).then(result => {

        if (!result) {
            return res.status(404).json({ statusCode: 'notFound', message: 'no se encontro perfil de usuario', });
        }
        bcrypt.hash(String(req.body.password), 10).then(function (hashedPassword) {
            database.usuario.update({
                where: {
                    username: req.body.username
                },
                data: {
                    password: hashedPassword
                }
            })
                .then((newUser) => {
                    // const newUser = { id: result.id, emailId:result.emailId, name: result.name, role: result.role};
                    console.log('password actualizado', newUser);
                    regenerateToken();
                    res.json({
                        statusCode: 'success',
                        message: 'contraseña actualizada correctamente!',
                    });
                })
                .catch(err => {
                    console.log('sending error', err);
                    return res.status(400).json({
                        statusCode: "badRequest",
                        message: "no se pudo modificar la contraseña",
                    })
                });
        });

    }).catch(err => {
        console.log(err);
        return res.status(500).json({ statusCode: 'serverError', message: 'error al actualizar informacion de usuario', });
    });
}
