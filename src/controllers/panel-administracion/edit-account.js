import database from '../../database/prisma-db-client.js';
export default async function editAccount(req, res) {
    let validFields = {
        username:null,
        nombres: null,
        apellidos: null,
        cargo: null,
    }
    database.usuario.findUnique({
        where: {
            rol: String(req.body.rol)
        }
    }).then(result => {

        if (!result) {
            return res.status(404).json({ statusCode: 'notFound', message: 'no se encontro perfil de usuario', });
        }
        let inputData = {}
        for (let key of Object.keys(validFields)) {
            if (req.body[key]) inputData[key] = req.body[key];
        }
        database.usuario.update({
            where: {
                rol: req.body.rol
            },
            data: inputData
        }).then(data => {
            return res.status(200).json({
                statusCode: 'success',
                message: 'datos modificados con exito, porfavor ingrese a tu cuenta nuevamente'
            })
        })

    }).catch(err => {
        console.log(err);
        return res.status(500).json({ statusCode: 'serverError', message: 'error al actualizar informacion de usuario', });
    });
}