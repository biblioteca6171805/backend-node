import database from '../../database/prisma-db-client.js';
export default async function getProfile(req, res) {
    database.usuario.findUnique({
        where: {
            rol: req.query.rol
        }
    }).then(result => {
        delete result.password;
        // result.message = "hello there";
        return res.status(200).json({ statusCode: 'success', message: 'perfil de usuario', data: result });
    }).catch(err => {
        console.log(err);
        return res.status(404).json({ statusCode: 'notFound', message: 'no se encontro perfil de usuario', });
    });
}