import database from '../../database/prisma-db-client.js';
export default async function searchByQuery(req, res) {

    if (Object.keys(req.query).length === 0) {
        return res.status(404).json({
            statusCode: 'notFound',
            message: 'no se encontraron coincidencias',
        })
    }

    let aceptableParameters = {
        titulo: String,
        tituloBusqueda: String,
        autores: String,
        editorial: String,
        encuadernacion: String,
        clasificacion: String,
        // aEdicion: Number,
        fechaLanzamiento: String,
        idioma: String,
        isbn: String,
        numPaginas: String,
        dimensiones: String,
        pesoFisico: String,
        signatura: String,
        notaGeneral: String,
        notaDeContenido: String,
        notaGeneral: String,
    }
    console.log(req.query);

    let sanitizedQuery = {};

    for (let key of Object.keys(aceptableParameters)) {
        if (req.query[key]) {
            sanitizedQuery[key] = {
                contains:
                    aceptableParameters[key](req.query[key]),
            }
        }
    }
    // console.log(sanitizedQuery);
    if (Object.keys(sanitizedQuery).length == 0) {
        let iddict = {
            id: Number,
        }
        for (let key of Object.keys(iddict)) {
            if (req.query[key]) {
                sanitizedQuery[key] = iddict[key](req.query[key])
            }
        }
    }

    console.log(sanitizedQuery);

    if (Object.keys(sanitizedQuery).length == 0) {
        return res.status(400).json({
            statusCode: "badRequest",
            message: 'parametros insuficientes',
        })
    }

    database.libro.findMany({
        where: sanitizedQuery
    }).then(async (libros) => {

        let page = Number(req.query.page);
        let limit = Number(req.query.limit);
        if (!(req.query.page && req.query.limit)) {
            return res.status(400).json({
                statusCode: "badRequest",
                message: 'parametros insuficientes',
            })
        }
        return res.status(200).json({
            statusCode: 'success',
            message: 'libros encontrados',
            data: {
                resultadosTotales: libros.length,
                libros: libros.slice(limit * (page - 1), limit * (page))
            }
        })
    })
        .catch(err => {
            console.log(err);
            return res.status(500).json({
                statusCode: "serverError",
                message: "error interno del servidor"
            })
        })


}