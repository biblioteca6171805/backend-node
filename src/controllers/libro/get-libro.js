import database from '../../database/prisma-db-client.js';
import { getEjemplaresById } from "../../services/sigb-unap.js";
export default async function getLibro(req, res) {


    database.libro.findUnique({
        where: {
            id: Number(req.params.libroId)
        }
    }).then(async (libro) => {
        if (!libro) {
            return res.status(404).json({
                statusCode: 'notFound',
                message: 'no se encontraron coincidencias',
            })
        }
        libro.ejemplares = await getEjemplaresById(libro.id);
        libro.ejemplares = libro.ejemplares.ejemplares;
        return res.json({
            statusCode: 'success',
            message: 'libro encontrado',
            data: libro
        })
    })
        .catch(err => {
            console.log(err);
            return res.json({
                statusCode: "serverError",
                message: "error interno del servidor"
            })
        })
}