import database from '../../database/prisma-db-client.js';
import { getEjemplaresById } from "../../services/sigb-unap.js";
export default async function getSeparacionById(req, res) {

    database.separaciones.findUnique({
        where: {
            id: String(req.params.separacionId)
        },
        include:{
            libros:true,
        }
    }).then(async (separacion) => {
        if (!separacion) {
            return res.status(404).json({
                statusCode: 'notFound',
                message: 'no se encontraron datos de la separacion',
            })
        }
        return res.json({
            statusCode: 'success',
            message: 'separacion encontrado',
            data: separacion
        })
    })
        .catch(err => {
            console.log(err);
            return res.json({
                statusCode: "serverError",
                message: "error interno del servidor"
            })
        })
}