import { validationResult } from 'express-validator';
import database from '../../database/prisma-db-client.js';

import bibliotecaParametros from '../../config/server.js'

export default async function evaluarSeparacion(req, res) {
    /*
    Entrada
 
    {
     separacionId:dada0ad0add-ad-a-da-da-dadada,
     nuevoEstado: enEspera | realizado | cancelado
    }
    */
    database.separaciones.update({
        where:{
            id: String(req.body.separacionId)
        },
        data: {
            estadoSeparacion:String(req.body.nuevoEstado)
        },
        include: {
            libros: true,
        }
    }).then(separacion => {
        return res.json({
            statusCode: "success",
            message: "separacion evaluada exitosamente!",
            data: separacion
        })


    }).catch(err => {
        console.log(err);
        return res.status(500).json({
            statusCode: "serverError",
            message: "error en db",
        })
    })
}