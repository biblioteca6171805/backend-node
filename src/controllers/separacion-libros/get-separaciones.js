import database from '../../database/prisma-db-client.js';

export default async function getSeparaciones(req, res) {

    let estados = ['enEspera','realizado','cancelado'];
    let estado = String(req.query.estadoSeparacion);
    if (!estados.includes(estado)) {
        console.log({ message: `estado ${estado} no valido` })
        return res.status(400).json({
            statusCode: "badRequest",
            message: `estado ${estado} no valido`,
        })
    }
    // estado ok
    database.separaciones.findMany({
        where: {
            estadoSeparacion: estado // if estado == undefined => estadoSeparacion All
        },
        orderBy: [{
            updatedAt: 'desc'
        }],
        include: {
            libros: true
        }
    }).then(data => {
        console.log(200, data.length);
        return res.json({
            statusCode: "success",
            message: estado ? `lista de separaciones con estado: ${estado}` : "lista completa de separaciones",
            data: data
        })
    }).catch(error => {
        console.log({ error: error })
        return res.status(500).json({
            statusCode: "serverError",
            message: "error al buscar las separaciones en db",
        })

    })
}