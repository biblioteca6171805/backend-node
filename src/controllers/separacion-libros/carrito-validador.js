import { check } from 'express-validator';
// import { check, validationResult } from 'express-validator';

let solicitudValidator = [
    check('dni').not().isEmpty(),
    check('codigoEstudiante').not().isEmpty(),
    check('libros').not().isEmpty(),
]


export default solicitudValidator;