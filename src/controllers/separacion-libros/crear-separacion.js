import { validationResult } from 'express-validator';
import asyncShell from '../../utils/async-shell.js';
import fs from 'fs';
import { v4 as uuidv4 } from 'uuid'
import path from 'path';
import database from '../../database/prisma-db-client.js';

import bibliotecaParametros from '../../config/server.js'

export default async function crearSeparacionLibros(req, res) {
    /*
    Entrada
    {
     dni:"70707070",
     codigoEstudiante: "121212",
     libros:"132151;312512;616141;4141512"
    }
    */
    let errors = validationResult(req).array();
    let data = req.body;
    data.dni = String(data.dni);
    data.codigoEstudiante = String(data.codigoEstudiante);
    data.libros = data.libros.split(';').map((val) => Number(val))
    if (errors.length > 0) {
        console.error(400, errors.length);
        return res.status(400).json({
            statusCode: "badRequest",
            message: "error en la validacion de datos",
            details: errors
        });
    }

    if (data.libros.length > bibliotecaParametros.biblioteca.cantidadMaximaDeLibrosASepararPorEstudiante) {
        return res.status(400).json({
            statusCode: "badRequest",
            message: "la cantidad de libros excede los parametros establecidos por la biblioteca de la escuela profesional",
            details: `cantidad maxima: ${bibliotecaParametros.biblioteca.cantidadMaximaDeLibrosASepararPorEstudiante}`
        });
    }

    let libros = []

    for (let libroId of data.libros) {
        libros.push({ libroId: libroId })
    }
    // verificar restriccion de una sola separacion
    database.separaciones.create({
        data: {
            dni: data.dni,
            codigoEstudiante: data.codigoEstudiante,
            libros: {
                create: libros
            }
        },
        include: {
            libros: true,

        }
    }).then(separacion => {
        // console.log(separacion);
        return res.status(200).json({
            statusCode: "success",
            message: "separacion realizada exitosamente!",
            data: separacion
        })


    }).catch(err => {
        console.log(err);
        return res.status(500).json({
            statusCode: "serverError",
            message: "error en db",
        })
    })
}