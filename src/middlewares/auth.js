// const { db } = require('../db');
import jwt from 'jsonwebtoken'
import 'dotenv/config'

// Resto de tu código aquí

import database from '../database/prisma-db-client.js';
const _SecretToken = process.env.JWT_SECRET
const _TokenExpiryTime = process.env.JWT_TOKEN_EXPIRE_TIME;

const authorize = function (roles = []) {
  if (!Array.isArray(roles)) roles = [roles];

  return (req, res, next) => {
    function sendError(msg) {
      let jsonResponse = {
        statusCode: 'forbidden',
        message: msg,
      }
      console.log(jsonResponse);
      return req.res.status(403).json(jsonResponse);
    }

    try {
      const token = req.headers["Authorization"] || req.headers["authorization"];

      if (!token) return sendError("No Token"); // Token does not exist
      if (token.indexOf("Bearer") !== 0) return sendError("Token format invalid"); // Wrong format

      const tokenString = token.split(" ")[1];
      jwt.verify(tokenString, _SecretToken, (err, decodedToken) => {
        if (err) {
          console.log(err);
          return sendError("Broken Or Expired Token");
        }
        console.log('decoded token', decodedToken);

        if (!decodedToken.rol) return sendError("Role missing");
        const userRol = decodedToken.rol;
        if (roles.indexOf(userRol) === -1)
          return sendError("User not authorized");

        req.user = decodedToken;
        // console.log('middleware passed')
        next();
      });
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        statusCode: 'serverError',
        message: "error del servidor",
        // err: err
      });
    }
  };
};

const issueToken = function (user) {
  var token = jwt.sign({ ...user, iss: "Node-Auth" }, _SecretToken, {
    expiresIn: _TokenExpiryTime,
  });
  return token;
};

const Roles = {
  Admin: ["admin"],
  Jefe: ["jefe"],
  Biblioterario: ['bibliotecario'],
  User: ["estudiante"],
  All: ["estudiante", "admin", 'jefe', 'bibliotecario'],
}

export default {
  authorize,
  issueToken,
  Roles
}