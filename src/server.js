import app from './app.js'
import 'dotenv/config'

const port = process.env.PORT ? parseInt(process.env.PORT) : 3000;
const host = process.env.HOST ?? '127.0.0.1'

app.listen(port, function () {
  console.log(`Express Server initiated listening on ${host}:${port}`);
});

process.on("SIGTERM", function () {
  console.log(`SIGTERM signal received: closing HTTP server.`);
  process.exit();
});

process.on("SIGINT", function () {
  console.log(`SIGINT signal received: closing HTTP server.`);
  process.exit();
});
