import axios from "axios";
import { parseHTML } from 'linkedom';
export async function getEjemplaresById(libroId){
    let url = `https://biblioteca.unap.edu.pe/opac_css/index.php?lvl=notice_display&id=${libroId}`;
    let page = await axios.get(url);
    const { document } = parseHTML(page.data);
    let selector =`#el${libroId}Parent > table.exemplaires`;
    console.log(selector);
    let bookTable = document.querySelector(selector);
    let output = {}
    output.ejemplares = [];

    let ejemplares = bookTable.querySelectorAll('tr.item_expl');

    for (let e = 0; e < ejemplares.length; e++) {
        let ejemplar = {}
        ejemplar.codigo = ejemplares[e].querySelector('td:nth-child(1)').innerText;
        ejemplar.signatura = ejemplares[e].querySelector('td.expl_cote').innerText;
        ejemplar.tipoDeMedio = ejemplares[e].querySelector('td.tdoc_libelle').innerText;
        ejemplar.ubicacion = ejemplares[e].querySelector('td.location_libelle').innerText;
        ejemplar.seccion = ejemplares[e].querySelector('td.section_libelle').innerText;
        ejemplar.estado = ejemplares[e].querySelector('td.expl_situation').innerText;
        output.ejemplares.push(ejemplar)
    }
    console.log(output.ejemplares);
    return output;
}
