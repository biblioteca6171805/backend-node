import { Router } from "express";
import * as auth from "../../controllers/auth/index.js";
import authMiddleware from '../../middlewares/auth.js';

const router = Router();

router.post('/login', auth.login);
// router.post('/create-account', auth.createAccount);
// router.post('/create-account',authMiddleware.authorize(authMiddleware.Roles.Admin), auth.createAccount);

export default router