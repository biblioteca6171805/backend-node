import { Router } from "express";
import * as libro from "../../controllers/libro/index.js";

const router = Router();

router.get('/get/:libroId',libro.getLibro);
router.get('/search/query',libro.searchByQuery);

export default router;