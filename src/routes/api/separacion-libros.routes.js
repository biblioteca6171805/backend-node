import { Router } from "express";
import * as auth from "../../controllers/auth/index.js";
import * as separacionLibros from "../../controllers/separacion-libros/index.js";
import authMiddleware from '../../middlewares/auth.js';

const router = Router();

router.post('/crear-separacion', separacionLibros.carritoValidador, separacionLibros.crearSeparacionLibros);

router.post('/evaluar-separacion', authMiddleware.authorize(authMiddleware.Roles.Biblioterario), separacionLibros.evaluarSeparacion);
router.get('/get/separacion/:separacionId', authMiddleware.authorize(authMiddleware.Roles.Biblioterario), separacionLibros.getSeparacionById);
router.get('/get/separaciones', authMiddleware.authorize(authMiddleware.Roles.Biblioterario), separacionLibros.getSeparaciones);

export default router