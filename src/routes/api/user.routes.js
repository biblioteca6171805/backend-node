import { Router } from "express";
import * as user from "../../controllers/user/index.js";
import authMiddleware from '../../middlewares/auth.js';

const router = Router();

router.get('/perfil', authMiddleware.authorize(authMiddleware.Roles.All), user.profile);
router.post('/editar-cuenta', authMiddleware.authorize(authMiddleware.Roles.Admin), user.editAccount);

export default router;