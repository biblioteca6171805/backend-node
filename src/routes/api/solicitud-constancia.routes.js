import { Router } from "express";
import * as solicitudConstancia from "../../controllers/solicitud-constancia/index.js";
import authMiddleware from '../../middlewares/auth.js';

const router = Router();

router.post('/solicitar-constancia', solicitudConstancia.solicitudValidador, solicitudConstancia.procesarSolicitudEntranteB64);
router.get('/ver-estado-solicitud', solicitudConstancia.verEstadoSolicitud);
router.get('/solicitud/:solicitudId/imagen', solicitudConstancia.getImage);

router.get('/solicitudes',  solicitudConstancia.getSolicitudes);
router.get('/solicitud/:solicitudId',  solicitudConstancia.getSolicitud);
router.post('/solicitud/:solicitudId/evaluar',  solicitudConstancia.evaluarSolicitud);
router.get('/solicitud/:solicitudId/pdf',  solicitudConstancia.getPdfFromSolicitud); // genera un pdf directamente con los datos de la tabla solicitud

export default router;