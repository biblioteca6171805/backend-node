import { Router } from "express";
import authMiddleware from '../../middlewares/auth.js';
import * as adminPanel from '../../controllers/panel-administracion/index.js';

const router = Router();

router.get('/get-profile', authMiddleware.authorize(authMiddleware.Roles.Admin), adminPanel.getProfile);
router.post('/edit-account', authMiddleware.authorize(authMiddleware.Roles.Admin), adminPanel.editAccount);
router.post('/update-password', authMiddleware.authorize(authMiddleware.Roles.Admin), adminPanel.updatePassword);

export default router;